from flask import Flask, request, make_response
import requests
from faker import Faker
from db import execute_query as query
from faker import Faker
fake = Faker()


app = Flask(__name__)


######################################################
@app.route("/customers_by_country")
def get_customers_by_country():
    country = request.args['country']
    result = query("""SELECT FirstName, LastName FROM Customers WHERE Country=?""", (country, ))

    result_list = []
    for item in result:
        result_list.append(str(item))

    response = make_response("\n".join(result_list))
    response.headers['Content-Type'] = 'text/plain'
    return  response


######################################################
@app.route("/bitcoin_rate")
def get_bitcoin_rate():
    res = requests.get('https://bitpay.com/api/rates').json()
    currency = request.args.get('currency', 'USD')

    for i in res:
        code = i.get('code')
        rate = i.get('rate')

        if code == currency:
            return str(rate)


######################################################
@app.route("/random_users")
def gen_random_users():
    users_list = []

    try:
        count = int(request.args.get('count', 100))
    except ValueError:
        return "Error..Count is not integer"

    for i in range(int(count)):
        user = fake.name(), fake.email()
        users_list.append(str(user))

    response = make_response("\n".join(users_list))
    response.headers['Content-Type'] = 'text/plain'
    return  response


######################################################
@app.route("/unique_names")
def get_unique_names():
    result = query("SELECT COUNT(DISTINCT FirstName) FROM Customers")

    return str(result[0][0])


######################################################
@app.route("/tracks_count")
def get_tracks_count():
    result = query("SELECT COUNT(*) FROM Tracks")
    # b = ''
    # for i in result:
    #     for k in i:
    #         b += str(k)

    return str(result[0][0])


######################################################
@app.route("/customers")
def get_customers():
    city = request.args.get('country', 'Norway')
    country = request.args.get('city', 'Oslo')

    result = query("SELECT * FROM Customers WHERE Country=? AND City=?", (city, country))
    print(result)
    result_list = []
    for item in result:
        result_list.append(str(item))

    response = make_response("\n".join(result_list))
    response.headers['Content-Type'] = 'text/plain'
    return response





app.run(debug=True)