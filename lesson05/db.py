import os
import sqlite3

def execute_query(query, args=''):
    conn = sqlite3.connect(
        os.path.join(
            os.path.dirname(__file__),
            'chinook.db'
        )
    )
    # conn.row_factory = sqlite3.Row
    cur = conn.cursor()
    result = cur.execute(query, args).fetchall()

    return result